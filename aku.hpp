///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file aku.hpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   16_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "fish.hpp"

using namespace std;


namespace animalfarm {

class Aku : public Fish {
public:
   float weight;

   Aku( float newWeight, enum Color newColor, enum Gender newGender );

   void printInfo();
};

} // namespace animalfarm
