///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   02_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
//#include <string>
#include <array>
#include <list>

#include "animal.hpp"
#include "mammal.hpp"
#include "bird.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "list.hpp"
#include "node.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   cout << "Welcome to Animal Farm 4" << endl;

///////////////////////////////                ///////////////////////////////
///////////////////////////////  Animal List   ///////////////////////////////
///////////////////////////////                ///////////////////////////////
   
   SingleLinkedList animalList;  // Instantiate a SingleLinkedList
   
   for( auto i = 0 ; i < 25 ; i++ ) {
      animalList.push_front( AnimalFactory::getRandomAnimal() ) ;
   }

   cout << endl;
   cout << "List of Animals" << endl;
   cout << " Is it empty: " << boolalpha << animalList.empty() << endl; 
   cout << " Number of elements: " << animalList.get_size() << endl;

   for( auto animal = animalList.get_first() // for() initialize
      ; animal != nullptr  // for() test 
      ; animal = animalList.get_next( animal )) { // for() increment
      cout << ((Animal*)animal)->speak() << endl;
   }
//debug
//cout <<  "DEBUG: reached delete loop" << endl;
//int i = 0;
   while( !animalList.empty() ) {
     /* //debug
      cout << "DEBUG: in loop, i = " << i++ << endl;
      cout << "head = " << animalList.get_first() << endl;
      cout << " Number of elements: " << animalList.get_size() << endl;
      */

      Animal* animal = (Animal*) animalList.pop_front();
      
      delete animal;
   }
}

