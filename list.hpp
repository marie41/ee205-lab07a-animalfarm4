///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// SingleLinkedList class header file 
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   30_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "node.hpp"

namespace animalfarm {

class SingleLinkedList {

protected:
   Node* head = nullptr;
   unsigned int size = 0;

public:
   //Constructor and destructor
   SingleLinkedList();
   ~SingleLinkedList();

   //Return true if the list is empty. false if there’s anything in the list.
   const bool empty() ;
   
   //Add newNode to the front of the list.
   void push_front( Node* newNode );
   
   //Remove a node from the front of the list. If the list is already empty, return nullptr.
   Node* pop_front();
   
   //Return the very first node from the list. Don’t make any changes to the list.
   Node* get_first() const;
   
   //Return the node immediately following currentNode.
   Node* get_next( const Node* currentNode ) const;
   
   //Return the number of nodes in the list
   unsigned int get_size() const;

};//end of SLL  class

};//end of namespace
