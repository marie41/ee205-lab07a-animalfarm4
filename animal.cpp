///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   02_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "node.hpp"

#define MAX_NAME_LENGTH (9)

using namespace std;

namespace animalfarm {

   Animal::Animal() {
      //prints . on creation of new animal
      cout << ".";
   }

   Animal::~Animal() {
      cout << "x";
   }

int randomNumber () {
   return rand() % 6; //random integer from 0 to 5
}

//returns random name of length 4-9. Expecting num between 0 and 5 
std::string randomName  (int num) {
   int length = num + 4;   //length of name is random integer between 4 and 9
   char name[ length + 1 ]; //declare name array, +1 size for '\0' termination  
   name[0] = 'A' + ( rand() % 26 ); //Add first letter, uppercase 

   for ( int i = 1; i < length ; i++ ) {
      name[i] = 'a' + ( rand() % 26 );
   }
   name[ length ] = '\0'; //making sure name is '\0' terminated

   return name;
}

enum Color randomColor() {
   enum Color color = (enum  Color) (rand() % 6); //random color generated from random integer between 0 and 6

   return color;
}

enum Gender randomGender() {
   enum Gender gender = (enum Gender) (rand() % 2); //gender is male or female

      return gender;
}

bool randomBool() {
   bool randomBool = (bool) (rand() % 2);

   return randomBool;
}

float randomWeight() {
   float weight = (float) (rand() % 20) + 70; // number between 70 and 90
   weight += ((rand() % 10) /10); //adds random after decimal number

   return weight;
}

//creates a random animal
Animal* AnimalFactory::getRandomAnimal () {
   Animal* pAnimal = nullptr;
   int random = randomNumber();
   int i = rand() % 6; //species is tied to random integer from 0 to 5

   //debug cout << "species = " << i << endl;
   switch (i){
      case 0: pAnimal = new Cat(      randomName( randomNumber() ),  randomColor(), randomGender() );
         break;
      case 1: pAnimal = new Dog(      randomName( randomNumber() ),  randomColor(), randomGender() );
         break;
      case 2: pAnimal = new Nunu(     randomBool(),                  randomColor(), randomGender() );
         break;
      case 3: pAnimal = new Aku(      randomWeight(),                randomColor(), randomGender() );
         break;
      case 4: pAnimal = new Palila(   randomName( randomNumber() ),  randomColor(), randomGender() );
         break;
      case 5: pAnimal = new Nene(     randomName( randomNumber() ),  randomColor(), randomGender() );
   }

   //DEBUG
   //pAnimal->printInfo();

   return pAnimal;

}

void Animal::deleteAnimal (Animal* byeAnimal) {
   delete byeAnimal;

   //prints x when animal is deleted
  // cout << "x" ;

}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE   :  return string("Male"); break;
      case FEMALE :  return string("Female"); break;
      case UNKNOWN:  return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK  :  return string("Black"); break;
      case WHITE  :  return string("White"); break;
      case RED    :  return string("Red"); break;
      case SILVER :  return string("Silver"); break;
      case YELLOW :  return string("Yellow"); break;
      case BROWN  :  return string("Brown"); break;
   }

	
   return string("Unknown");
};

} // namespace animalfarm
