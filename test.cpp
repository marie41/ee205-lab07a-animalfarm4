///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file test.cpp
/// @version 1.0
///
/// Tests node and singlylinkedlist classes (functions,etc)
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   30_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Hello World" << endl;
   
   //made protected constructor
   //Node node;  // Instantiate a node
   SingleLinkedList list;  // Instantiate a SingleLinkedList
   
   

	return 0;
}//end of main

