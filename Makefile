###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 06a - Animal Farm 3
#
# @file    Makefile
# @version 1.0
#
# @author Marie Wong <marie4@hawaii.edu>
# @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
# @date   16_FEB_2021
###############################################################################

all: main

test.o:  test.cpp
	g++ -c test.cpp

node.o: 	node.hpp node.cpp
	g++ -c node.cpp

list.o:	list.hpp list.cpp
	g++ -c list.cpp

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

factory.o:  factory.hpp factory.cpp
	g++ -c factory.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp node.cpp
	
mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp

fish.o: fish.cpp fish.hpp
	g++ -c fish.cpp

nunu.o: nunu.cpp nunu.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp
	g++ -c aku.cpp

bird.o: bird.cpp bird.hpp
	g++ -c bird.cpp

palila.o: palila.cpp palila.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp
	g++ -c nene.cpp

main: main.cpp *.hpp main.o node.o list.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o   
	g++ -o main main.o node.o list.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o 
 
test: test.cpp *.hpp test.o node.o list.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o 
	g++ -o test test.o node.o list.o animal.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o
 
	
clean:
	rm -f *.o main
