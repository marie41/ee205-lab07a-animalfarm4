///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// SingleLinkedList class file 
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   30_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include "node.hpp"
#include "list.hpp"
#include <iostream>

namespace animalfarm {

   SingleLinkedList::SingleLinkedList() {
      head = nullptr;
      size = 0;
   }

   SingleLinkedList::~SingleLinkedList() {}
   
   //Return true if the list is empty. false if there’s anything in the list.
   const bool SingleLinkedList::empty() {
      bool isEmpty = true;

      if (head == nullptr) isEmpty = true;
      else isEmpty = false;

      return isEmpty;
   }
   
   //Add newNode to the front of the list.
   void SingleLinkedList::push_front( Node* newNode ) {
      newNode->next = head;
      head = newNode;
      size++;
   }
   
   //Remove a node from the front of the list. If the list is already empty, return nullptr.
   Node* SingleLinkedList::pop_front() {
//DEBUG
//std::cout << "DEBUG1: waiting to  pop a node" << std::endl;     
      if ( head == nullptr) {
         return nullptr;
      } 
         //DEBUG
//std::cout << "DEBUG2: head != nullptr waiting to  pop a node" << std::endl;
         Node* temp = head;
         //DEBUG
//std::cout << "DEBUG3" << std::endl;

         head = temp->next;
//DEBUG
//std::cout << "DEBUG4" << std::endl;

         size--;
      return temp; 
   }
   
   //Return the very first node from the list. Don’t make any changes to the list.
   Node* SingleLinkedList::get_first() const {
      return head;
   }
   
   //Return the node immediately following currentNode.
   Node* SingleLinkedList::get_next( const Node* currentNode ) const {
      return currentNode->next;
   }
   
   //Return the number of nodes in the list
   unsigned int SingleLinkedList::get_size() const {
      return size;
   }


};//end of namespace
