///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
/// Node class header file 
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
/// @date   30_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

namespace animalfarm {

class Node {
   //allow SingleLinkedList to access Node's private and protected
   friend class SingleLinkedList;

protected:
   Node* next = nullptr;

   Node();
   ~Node();

};//end of Node class

};//end of namespace
