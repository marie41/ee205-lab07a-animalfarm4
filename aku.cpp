///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   16_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku( float newWeight, enum Color newColor, enum Gender newGender ) {
   gender = newGender;        /// Get from the constructor... not all aku are the same gender (this is a has-a relationship)
   species = "Katsuwonus pelamis";    /// An is-a relationship. All aku  are the same species.
   scaleColor = newColor;     /// A has-a relationship, so it comes through the constructor
   favoriteTemp = 75;         /// An is-a relationship.

   weight = newWeight;        /// A has-a relationship.
}


/// Print our Aku and name first... then print whatever information Fish holds.
void Aku::printInfo() {
   cout << "Aku" << endl;
   cout << "   Weight = [" << weight << "]" << endl;
   Fish::printInfo();
}

} // namespace animalfarm
