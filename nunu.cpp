///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 06a - Animal Farm 3 - EE 205 - Spr 2021
/// @date   16_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {

Nunu::Nunu( bool newNative, enum Color newColor, enum Gender newGender ) {
   gender = newGender;        /// Get from the constructor... not all nunu are the same gender (this is a has-a relationship)
   species = "Fistularia chinensis";    /// An is-a relationship. All nunu  are the same species.
   scaleColor = newColor;     /// A has-a relationship, so it comes through the constructor
   favoriteTemp = 80.6;       /// An is-a relationship.

   isNative = newNative;      /// A has-a relationship.
}


/// Print our Nunu and name first... then print whatever information Fish holds.
void Nunu::printInfo() {
   cout << "Nunu" << endl;
   cout << "   Is native = [" << boolalpha << isNative << "]" << endl;
   Fish::printInfo();
}

} // namespace animalfarm
